# De Alma Mater ad Negligens Mater

**Pridie Kalendas Martias, Anno Domini MMXXIII**

**Rescripta: Ante diem quintum Nonas Martias, Anno Domini MMXXIII**


## Summarium

I rant about how an obsession with counting scientific papers made
academia go from a nurturing mother to a negligent one. Still, due to
the tireless efforts of the many altruist academics, who care for
their students, academia remains a nurturing mother for her children.
Consider taking the time to thank the nurturing academics in your life.



## Genesis de Academia

Academia nata est in Platus hortus. Just kidding I'm not gonna write
the whole post in my horrible Latin... Just the headings. As I was
saying... [The term academia stems from the garden where Plato taught
his students.](https://www.etymonline.com/word/academia) Tracing the
entire history of academia is far beyond my abilities, but in Europe, to
the best of my knowledge, the oldest universities were founded by Kings
and recognized by the Pope. I'm not saying the gardens of ancient Greece
or the Medieval universities were perfect, but I think it is fair to say
that they weren't obsessing over outputs. Catering mostly to high-status
individuals, and having the patronage of Kings, probably didn't place
the universities in a position where they had to obsess over their
position in international rankings. Still, one could say that in some
corners of the world, particularly in the United States, universities
still cater to wealthy students (or their families), and continue to
have patrons with deep pockets. In other corners, as in European
countries, such as Portugal, many universities are publicly funded.
I guess that's not that different from being funded by the Crown (not
the TV show). Even in the United States, universities still get a lot of
funding from public grants. Obviously, there has been tremendous social
change from ancient Greece, and the Middle Ages, to today. Apart from
the Taliban and other extremists, nobody seems to be saying women should
not bee allowed to attend higher-education. Still, that isn't the social
change we'll focus on today. Let us look at how states started to rank
and fund universities, and consequently how universities select and pay
their academics.



## Problematum Problematorum

In evaluating its members, academia is obsessed with outputs. That is
the *real problem* that seems to cause all other problems in academia
(or in my horrible Latin *problematum problematorum*). Those outputs are
mostly defined as scientific papers. Books, patents, oral presentations,
also count, but they count much less than scientific papers. The quality
of a scientific paper is then graded by the impact factor of the journal
that publishes it, and by the number of times the paper is cited in,
guess what, other scientific papers. Likewise, a journal's impact factor
is also a function of how many highly cited papers it publishes (amongst
other factors). Academia is thus turned into a printing press that
measures its sales by the number of times its prints are cited in other
prints. Truth be told, the citations of a paper should correlate with
the number of times it was read, but much of this is probably amplified
by the fact that platforms like Google Scholar will sort by citation
count (amongst other factors). Importantly, the number of times the
paper is read by people outside the ivory tower, or by those (far too
few) in the ivory tower that disseminate science, is not really
a factor. This is nothing new to academics, nor even to many in the
general public.



## Focus

We could discuss the relatively recent history of academia's obsession
with outputs, or highlight initiatives that aim to change that. However,
there are others more qualified to that, and that's not the point of
this post. We could also debate in length the so called replication
crisis, but we'll also have to postpone that discussion for another day.
Instead, I'll *focus* on an (arguably) often neglected symptom effect of
the *problematum problematorum* (aka., the *real problem*)---the quality
of academia's teaching.



## Academia, Quid Facitis Filiis Tuus?

The university one graduated from is referred to as *alma mater*. *Alma
mater* translates to [nurturing
mother](https://www.merriam-webster.com/dictionary/alma%20mater).
However, I'm afraid that the obsession with publishing scientific papers
has shifted the focus from what students can learn in a university, to
how fast and how much can their professors write. Academia could be
a place where students have a chance to expand their world-view, to
learn about big ideas, to learn foundational skills. It can/should also
be a place where students learn the necessary skills to enter the job
market. Regardless, today, academia seems to be focused on neither of
those. Instead, academia seems to be neglecting her children due to an
healthy obsession (to have become a *negligens mater*). This is not to
say academia doesn't play any of those roles, but that academia is
obsessed with how many papers her professors publish, not with how
well she's preparing her student.


### Parodiam

Academia seems to have become a place where students come to take a lot
of time from their professors---professors that should be spending their
time writing papers. Why else should academics mostly be graded by the
number of scientific papers they publish? I guess senior academics were
publishing too much, not giving a chance for junior academics to
compete... Maybe that's why universities tried to slow senior academics
down with more teaching appointments... Maybe that's why some senior
academics try to get their PhD students to teach their classes. One
could think that senior academics teach because they have a better grasp
of their fields (than junior academics). One could think senior
academics sometimes offer the chance for junior academics to share their
new ideas, with new generations of students. That would make sense in an
academia focused on teaching students, and sharing knowledge with the
public. However, surely, if that was the case academics wouldn't be
graded merely by the papers they publish... Yet academics are graded
by how many papers they publish, and rarely by the quality of their
teaching. I guess universities must be really recruiting students only
to make publishing harder for academics, lest they break the printing
presses from too much printing (or crash the scientific journals'
servers from too much uploading).


### Justi et Pii Docent

I'm sorry for the negative tone of this post, and I do wish to end on
a positive note. I believe students are being protected by incredible
academics, who still believe academia is more than a printing press, and
who deeply care for them. Those righteous (*justi*) individuals work
dutifully (*pie*) to honor, not what academia tells them their duty is
(i.e., to publish), but what they believe their duty to be---to teach
(*docere*). To be clear, those amazing academics also do amazing
research and publish in top journals, they just refuse to focus solely
on that. I don't wish to make a clear cut binary distinction between
those who care for their students, and those who care just for their
career. I believe most care for both, and most go out of their way to
teach their students, and that's why the problem is not made worse.
Still, the *real problem* is that academics are having to go the extra
mile to do their job, and risk falling short on an unfair metric, that
poorly measures only a portion of their job. To end on a positive note
then. Let us all do our best to appreciate the amazing people who go out
of their way to teach, and care for the people entrusted to them. I owe
so much to so many amazing academics. They are the caregivers who pick
up the slack for the *negligens mater*, allowing academia to continue to
be an *alma mater* for so many students. To them my gratitude. To them
I dedicate this post.



## Gratias

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
