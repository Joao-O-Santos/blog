# Job Crafting

**Date: 2023-02-14**

**Edited: 2023-02-23 (to address issue #1)**

**Many thanks for the feedback, Cristina!**


## Summary

Have you tried tweaking your job to your needs and personal preferences?
In this post, I discuss how I have done it from my masters' until now.
I know I've been particularly privileged to have had people supporting
me throughout the process, and maybe even in having work and interests
that lend themselves well to job crafting. Still, I would like to
suggest you give job crafting a try, or at least consider it, as a means
of improving your work experience.



## What's Job Crafting?

Wrzesniewski and Dutton (2001) coined the term job crafting to refer to
how *"employees craft their jobs by changing cognitive, task, and/or
relational boundaries to shape interactions and relationships with
others at work."*. Note that Wrzesniewski and Dutton (2001) highlight
how job crafting can occur at the cognitive, task, or relational level.
Meaning, people can tweak how they see their job and the impact it has
on them (cognitive), how they perform their tasks (task), or how and
whom they interact with (relational). I know nothing of this field, and
this post is not a review (for that see Tims et al., 2022; Zhang
& Parker, 2019), so this introduction will have to do.



## My Attempts at Job Crafting


### Past

I may have been job crafting when I chose topics that I like for
assignments, and got a kick out of presenting group projects in unique
ways, but I would say I started job crafting in the last year of my
masters. At that point I was becoming more of a `Linux` geek every day,
and I was trying to do everything in the command line. Not much has
changed in that regard... I'm still learning to do that by trial and
error. However, back then, as my advisor [Sara
Hagá](https://orcid.org/0000-0003-4957-4702) must recall, there were
many more errors than successful trials, so many errors... Sara is as
tolerant of my failures as one can be, much more than she should, so she
let me continue to try...and fail... My `LaTex`-written master thesis
was far from being 100% APA compliant... [Tomás
Palma](https://orcid.org/0000-0003-2936-4732), in my thesis defense,
noted he believed the thesis was only poorly formatted because of
`Linux`. To this day my parents remind me of that whenever I speak of
how great `Linux` is. In actuality, if I'm not mistaken, the most poorly
formatted portion was the reference section, and I wrote that manually
in a word processor (not `LaTeX`)... I started doing my analysis in `R`,
but at the time I didn't quite understand how to do ANOVAs in `R`,
especially if they were *repeated measures* ANOVAs. I also tracked my
files with `git`, which I don't believe caused many problems, but I also
don't think was that helpful at the time. I do recall committing the
`pdf` versions of the files besides the `LaTeX` version, and how my
friends studying computer science scoffed at it...

Anyway... After my masters I jumped right in to my PhD (thanks to Sara's
tireless efforts). I continued to learn by trial and many errors until
I eventually got a good grasp of how to do all my analysis in `R`.
I even learned how to run linear mixed models, write `RMarkdown`
reports, and many other wonderful techniques that have greatly improved
my workflow. I also volunteered to assist my colleague, [Cristina
Mendonça](https://orcid.org/0000-0002-1814-5270), in creating the
website for our PhD programme, which allowed me to learn about HTML,
CSS, and (very) basic web development. I should have noted this before
but I got increasingly drawn into philosophy of science, research
methods, getting sucked into the replication crisis debates. With the
help of my co-advisor, [Leonel
Garcia-Marques](https://orcid.org/0000-0003-0800-7664), and with Sara's
wise advice, I managed to turn what should have been a class assignment
(that was never submitted) into a manuscript the three of us have been
working on. To avoid getting called out for APA compliance again, I've
written a custom template for `pandoc` (see [my
dotfiles](https://gitlab.com/joao-o-santos/dotfiles)) so I can write in
`pandoc`-flavored `markdown`, and render it to a (mostly) APA 7th
compliant `.docx` file. In the end, something amazing happened. People
started to hear that I could do nice things with `R`, and they started
asking for my help.


### Present

This leads me to today. For over a year now, I've been making a living
with tutoring, giving workshops, consulting in statistics and in `R`.
[RUGGED](https://rggd.gitlab.io)--an `R` user group I helped create and
coordinate--was born after one of my workshops (I plan to write even
more about [RUGGED](https://rggd.gitlab.io) in the future, stay tuned).
My passion for programming, `git`, `Linux`, `Make`, and technology in
general has led me to learn enough so I could use GitLab CI/CD, and
`Docker` to automate my work. One of the
[projects](https://rggd.gitlab.io/projects.html) I'm spearheading over
at
[RUGGED](https://rggd.gitlab.io)---[SciOps](https://gitlab.com/rggd/SciOps)---aims
to provide a useful template to simplify the use of CI/CD tooling for
open science.

Getting back to job crafting... In one of my freelancing gigs I found
a way to partially automate a very repetitive and boring task, by doing
some web scrapping in `R`. I'm also happy to report I found the
opportunity to give a workshop on open science where I got to introduce
[SciOps](https://gitlab.com/rggd/SciOps), and hope to present it again
soon. I've also been volunteering to create websites, just so I can use
[make it\_stop](https://gitlab.com/Joao-O-Santos/make-it-stop)---a
static website generator I started developing after finding out about
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).


### Future

I obviously don't know the future, and as someone who's trying to finish
his PhD, while working as a freelancer, my future looks more uncertain
than it has in a while. Still, I expect whatever I end up doing I'm
going to try and:

1. continue to work in the command line as much as I can, and I will
   likely also continue to highlight the importance of research methods
   (task crafting);

2. find good mentors (relational crafting);

3. see myself as someone who can make a boring task fun, and who is jack
   of all trades master of none, that can oftentimes be better than
   a master of one (cognitive crafting).



## Why Job Craft?

I know this is my blog, but enough about me. I just wanted to give you
some examples of what job crafting can be like, but I ended up
reminiscing, for way too long... The only point I wanted to get across
is that sometimes a job can be boring, or it can be great while you
still feel something is missing for you. In that case, perhaps there's
something you can do to make it more interesting to you, to make it
scratch that itch that you have, that perhaps you only get to scratch
with a hobby. Let's say you love learning languages, but work in
a country and with people that only speak a language. Well maybe if you
have to google something for your work you can write your queries in
a different language. Maybe you can find a client, a supplier, or what
have you that's from another country and you can get to speak another
language with them. If there is something that you love that you think
has no relation to your work, think again, maybe there's a connection
waiting to be made. I'm obviously not guaranteeing that's always gonna
be the case, nor do I want to sound tone deaf to anyone stuck in a job
they hate, and want to get out of, but need it to pay the bills. I'm
just saying that maybe if you've never thought about job crafting,
consider giving it a try, you might enjoy it!



## Thank You

**Thanks you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
