# To Dad

**Date: 2023-03-19**

Thank you dad for all you have done for me! Thank you so much for all
you have done with me! Thank you for all the times you helped me with my
homework, for all the times we play together, for all the times we're
together, and for all the times we're apart but I know I can count on
you. Thank you dad for being a reflection of The Father's mercy and
kindness.

Thank You, Heavenly Father, for my dad, thank You for St. Joseph,
thank You for all the dads that sacrifice so much for their children,
please help those who have failed to show their love, please help us all
to be a better image of You.

St. Joseph, please, pray for us all, through your intercession, and that
of your wife, St. Mary, may God protect our children, our families, and
help us care for them.
