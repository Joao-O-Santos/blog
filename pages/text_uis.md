# Are Text-Based UIs Making a Comeback?

**Date: 2023-02-10**


## Summary

Are text-based user interfaces (UIs) making a comeback with chat bots
and chat-based AIs? As a command line `Linux` geek I'm left wondering if
we might be getting back to text-based UIs, with the rise of chat bots,
and AIs like chatGPT that let us chat with it.


## In Praise of the Command Line

In my `R` workshop I usually cite and adapt [this
koan](http://www.catb.org/esr/writings/taoup/html/gui-programmer.html),
from Eric S. Raymond's [The Art of UNIX
Programming](http://www.catb.org/esr/writings/taoup/). It's a really
short passage, so you should go read the original... The takeaway from
the koan is that graphical user interfaces (GUIs) might be easier to
learn and use, when you know nothing about working a given software.
However, GUIs don't really allow for mastery, you're likely as efficient
working with them after a year, than you were after the first week or
month. GUIs are typically less stable in terms of how frequently they
get changed, cosmetically, but also in regards to where everything is
positioned. Command line interfaces (CLIs), on the other hand, have
managed to stick around (just like UNIXes) for a long time, and to
(mostly) stay backwards compatible. This means someone's shell scripts
from a decade ago can likely still run in the latest version of `bash`.
CLIs allow you to talk to your machine, using very few short "words".
They allow to reach ever new levels of mastery, including finding ways
to continuously automate routine tasks. However, if you ignore us
die-hard `Linux` users, and/or programmers, you see CLIs have largely
been replaced by GUIs. **In short, it seems GUIs have won, and
text-based interfaces have lost.**


## The Comeback?

Today [a LinkedIn post from Diana
Orghian](http://www.catb.org/esr/writings/taoup/html/gui-programmer.html)
has left me wondering... With the rise of chat bots, and AIs like
chatGPT, are we getting back to interacting with our machines through
text? Are finding it easier, even superior, to interact with AIs by
writing to tell them what we want, and how we want them to revise what
they've generated? Admittedly, AIs don't force users to write their
queries only in a very specific way, returning syntax errors if they
don't. Yet, isn't that just the Unix way of programs, following the
[robustness
principle](https://en.wikipedia.org/wiki/Robustness_principle), being
liberal with what they accept as input, and strict about what they
return as output, taken to new heights? **In short, are text-based
interfaces winning the war, after having lost a battle?**


## Disclaimer

I'm not a UX researcher, nor a UI designer. I don't find myself the
greatest UNIX historian, nor an amazing CLI user. Still, I've lived in
the terminal long enough to ask myself: are people rediscovering the
value of text-based UIs?


## Thank you

**Thank you so much for reading!**
*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
