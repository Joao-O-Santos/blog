# Android, (Real) Linux, and "The Year of the Linux Desktop(tm)"

**Date: 2023-09-29**


## Summary


## A Wild LTT Appears

I had heard Linus Sebastian's take on Chrome OS on LTT's WAN show.
Recently, tough LTT devoted an entire video to how Chrome OS may be
gaining marketshare without people noticing.

<iframe width="560" height="315" src="https://www.youtube.com/embed/EE7bed5vD0Y?si=BFspKtWhF0kXFhdh"
 title="YouTube video player" frameborder="0" allowfullscreen></iframe>


## The Year of the Linux Desktop

There's a famous meme where people argue that *this* year will be the
year of the Linux desktop (where *this* = whatever year it is). Meaning,
it will be the year when Linux gains mass adoption on the desktop. When
the year comes to an end, and Linux does not dominate desktop market
share, a new year comes, and *that* year becomes the year of the Linux
desktop. The meme has resulted in Linux users simply wishing happy year
of the Linux desktop instead of happy new year.


## Why Doesn't Linux Dominate the Desktop?

Linux has dominated the supercomputer market, the phone market (Android
runs on Linux, more on that soon), the appliances market, if it has a
CPU, chances are Linux will run on it, or will soon be ported to run on
it. Why hasn't Linux dominated the desktop, then?


### Common Theories

From the top of my head I gather there are a few informal theories.

- **It's Too Hard to Use:** Linux has a fame for being command line only
  and an OS only *l33t hax0rs* can use. It also has a fame of having
  poor hardware support turning an installation into a kernel debugging
  session.

- **Toxic Community:** Others point out that the community (or
  communities) of linux users can be quite toxic and unwelcoming to
  newcomers, acting as rude gatekeepers.

- **Anti-Profit Ideology:** The Linux community, and the free software
  community in general, have the reputation of being anti-profit
  socialists or communists. People then argue that this ideology,
  coupled with the low marketshare, scares corporations away from
  investing in Linux.

- **Fragmentation:**

- **It Doesn't Come Pre-Installed:**



## What About Android?


### It's Noob-Friendly

Android shows Linux can be very newcomer-friendly (noob-friendly if you
will). Sure, us Linux users, don't really like Android for all it has
done to invade people's privacy, or for how it doesn't seem to care
about free and open source. Still, Android runs on Linux, and I don't
think it's user friendly because it replaced `bash` and `coreutils` with
`busybox`, or some other low-level aspect. Sure it is built for touch
interfaces, but many desktop environments can support that too.
Moreover, there's Android TV which works well with a remote, and there's
Android auto, and there would probably be a lot of Android internet of
things devices if that hype train had ever arrived at the station.


### Does it Have a Community?

It does have a community of users and developers, but a lot of people
use Android with 


### It's Corporate All the Way?


### Is it Consolidated?


### It Comes Pre-Installed



## What if Linux Came Pre-Installed?


### SteamOS


### HP DevOne


### ChromeOS
