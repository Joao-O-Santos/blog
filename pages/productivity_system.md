# My Productivity System

**Date: 2023-mm-dd**


## Summary



## Productivity/Organization Systems


### TODO-Lists

Super simple and incredibly powerful, this will be familiar to most
people. Still, I know I think I spent a really long time without even
writing down the tasks I had to do. Sometimes you might think you don't
need to write down your tasks because you know what you have to do. I
don't think that's very wise... You might have such a good memory and
focus that you can remember all your tasks throughout the day, week, or
even the year. The thing is that by not writing things down, you're
putting yourself in place where you **have** to remember all your tasks.
I am sure that there are better uses for your cognitive resources than
remembering all your tasks.

[Warning: rant incoming!]

Socrates is said to have disliked writing, because it would make people
dumb, not remember things, and loose any ability to
communicate---rhetoric (to use a greek term). Sidenote, we humans do
seem to keep complaining about how new tech will ruin new generations...
Socrates was a great philosopher but I think it was wrong about this
one. Could you think how life would be without the ability to read and
write? I'm not talking about not reading nor writing books. I mean any
form of writing; not even those cute modern hieroglyphs called emoji.
Everybody would have to remember everything all the time. Maybe Socrates
was right and our memory would be better if we were not addicted to this
technology of writing. Then again, if we know Socrates thoughts today
was because Plato was kind enough to write them down...

[End of rant]

Anyway, if you haven't tried it yet, please do write down your tasks and
what's in your mind (plans, ideas, etc...). If you're anything like me
you'll feel such a clarity of mind. I know it sounds like a drug, maybe
it is. Maybe Socrates was right and writing is just an addictive
technology, and society always went downhill from there...


### KanBan

Born out of Japan's car factory assembly lines, KanBan is a system that
helps you visualize your work. With KanBan you can clearly see what
you've done, what you're working on, what you should work on next, and
other tasks you may need to use. The general ideas of the system are so
simple and powerful that you can adapt it to help you visualize the
tasks assigned to each team member, the tasks assigned to different
teams, or group your tasks by topic.

### Timeboxing

If you haven't heard about time boxing, please don't be scared I'm not
gonna write about actually boxing with time. I'm not that high on
writing (or any other drug for the record). Though boxing with time
would be a cool premise for an epic poem of sorts... Anyway... You're
not gonna box with time, that's a fight nobody wins... Timeboxing is
just the idea of creating boxes of time, or calendar slots (to use a term
that actually makes sense), that you assign to tasks as if you were
setting up a meeting or some other appointment. Yeah, that's pretty much
it. It's like you set up a meeting with yourself to work on something
you want (and/or have) to work on. This is particularly important for
protecting, or making out time (not make out with time... which is a
premise for a different poem) to work on those tasks that you really
need to work on but keep putting of because something more urgent comes
up (see Eisenhower quadrant section below).


### Pomodoro Technique

The Pomodoro technique is *"closely related to concepts such as
timeboxing [...]"*
[Wikipedia](https://en.wikipedia.org/wiki/Pomodoro_Technique).

<!-- TODO: add something about flow state -->

I'd say the pomododoro technique is both more flexible and,
unintuitively, also more structured than regular timeboxing. More
flexible because you try to estimate how many pomodoros a task will take
ahead of time, but you can keep working on it for more pomodoros, before
you move on to the next task. Whereas, if you had setup a calendar slot,
such as with regular timeboxing, you could feel compelled to move on to
the next task as to not be "late for that appointment". Note that this
can be seen as pro or con of either approach. Easily allowing yourself
to allot more time for a task can lead you to neglect the others, which
could potentially be more important and/or urgent (see Eisenhower
quadrant below). At the same time, feeling like you're late for a task
before you're done with it, maybe while you're in a flow state
(CN[](<!-- TODO: add some ref on flow state)), is a really good way to
step out of that flow state, and stress yourself out feeling like you're
failing your goals.

<!-- TODO: Note on 52/17 and other similar systems -->

<!-- TODO: warn against customizing pomodoro session times, and link to
keept it OG section below -->

<!-- TODO: transition -->

Pomodoro breaks seem like you're wasting work time, but they
make you step out, and maybe gain a perspective, or do something that is
good for your health versus just siting on the same chair for longer.
Traditionally, people who work full time jobs also take coffee breaks,
or smoke breaks, and I don't see companies deducting that time, so you
should'nt feel bad about taking breaks if you're a Msc/PhD student,
freelancer, and/or remote worker. Also, if only smokers get to take
short breaks, we're rewarding smoking, which doens't seem like a very
good idea...



### Systems/Frameworks



#### GTD

**If something can be done in less than five minutes, just do it, it's
not worth logging.** This seems like a really useful tip. I would just
caution that this could also lead you to missing out on more important
but larger tasks if you get many of these short tasks, namely, if you
have a lot of emails/texts/chats you can quickly reply to.


### Tiers



## Setting Goals/Budgeting Time

### The Eseinhower Matrix

- Input- vs. Output-based goals
- You probably want both
- For boring tasks you want more input-based goals
- For engaging/distracting tasks you also want input-based stops
- For rewards you'll want input-based only for boring tasks
- Otherwise you'll want to award and measure outputs
- Revise to make sure you're picking and measuring relevant outputs



## My System


### Version X - GUI

I was having a good time with [Super
Productivity](https://super-productivity.com/). I noticed it could
import issues from GitLab and GitHub. That made me start moving away
from having a `TODO.md` per project and simply using GitLab's issue
system. My quest to find an organizational system continues. I've since
abandoned `Super Productivity`, and have been trying
[taskwarrior](https://taskwarrior.org), coupled with
[bugwarrior](https://github.com/ralphbean/bugwarrior), which pulls my
GitLab and GitHub projects' issues into `taskwarrior`. To be honest
I don't know if I'll stick with `taskwarrior+bugwarrior`. Most of my
projects are already in GitLab. This means, I can just go to that
project's directory/`git repo` and run `glab issue list`. There, `glab`
can also help me sort through the issues. GitLab has an amazing feature
called issue boards, allowing you to create
[KanBan](https://en.wikipedia.org/wiki/Kanban) style boards with your
issues. Using `glab issue board view` is all I need to see those boards
without leaving the terminal. Since I was already trying to use
KanBan+[GTD](https://en.wikipedia.org/wiki/Getting_Things_Done), this
solution seems like a dream come true. I'm just missing two things:

- I need to find a way to prioritize, and decide in which project I'm
  going to work on next.

- I need to have a way to manage simple tasks that are not part of
  projects. For instance, sending emails.

[Habitica](https://habitica.com) is still a cool gamification tool to
boost my motivation. It works well for managing simple tasks, and I can
also add project milestones there to get a ton of positive reinforcement
upon reaching them. Still


### Version Y - CLI

#### GitLab

- Issues, with assignees and labels (common, and project specific)
	+ "DOING" and "NextUp" as labels common to pretty much
	  all projects.
	+ Boards with a "NextUp" and "DOING" column.

- Quick actions in `git commit` messages to close issues (e.g., "Closes
  #9")

- `glab` for interacting with issues in the command line
	+ `glab issue [list/create/update/close`
	+ `glab ci view` used sometimes to view the status of a pipeline

#### Taskwarriror+Bugwarrior

- `bugwarrior-pull` to pull all issues from all GitLab and GitHub
  projects.

- `task add [task description]` for adding smaller tasks that are not
  part of projects (e.g., emails).

- `task context doing` for a custom context that selects tasks marked
  with the "DOING" label and assigned to me on GitLab, as well as, all
  small tasks (tasks without a project).

- `task context picked` for a custom context that selects tasks that
  I've marked with the tag "pick" (e.g., `task 43 modify +pick`, to tag
  task 43 with the "pick" tag)

- `bugwarrior-pull` to mark issues closed in GitLab (through the Web UI,
  through quick actions, or with `glab`) as completed tasks in
  `taskwarrior`.

- `task X done` to mark tasks without a project as completed.

#### pomofocus.io

I use [pomofocus.io](https://pomofocus.io) without an account or memory
from previous sessions, just to assign pomodoros and order the tasks
I've picked for the day. I would like to replace pomofocus with super
productivity or some other open source tool, maybe command line based.


### Present - Bullet Journal

#### Pomodoro - revisited

(for creative and artistic uses of a bullet journal with the pomodoro
technique see [Park,
2017](https://www.evydraws.com/blog/2017/12/16/productivity-tip-pomodoro-technique-bullet-journal))



### Future?



## Your System


### Pick a System/s


### Keep it Stupid Simple

Keep it OG before modifying


### 1m of Planning = -3h of Work and Confusion

### 1000h of Planning != 1h of Work

### Stick to It

### Make it Your Own

- Customize the length of work sessions and breaks pomodoros

- Try out different tools

- Add features and complexity incrementally and only if the need arises


### Resources

- Productivity techniques wikipedia <!-- Link to relevant wiki pages
  categorizing systems -->
- Pomodoro Technique (CN; CN) <!-- TODO: Link to official book and website -->
- GTD (CN; CN) <!-- TODO: Link to official book and website -->


## Take-Home Message

Organizing your work can have tremendous impacts not only for your
productivity, but also for your mental-health and work-life balance.



## Thank you

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
