# Rhythm and Rest

**Date: 2023-04-05**



## Summary

In this post, I reflect on the consequences of no longer having rest as
something sacred, and no longer observing periods of time that give
a sense of rhythm to our year. I speak from a Catholic perspective, but
I hope the general sentiment can resonate with you, regardless of your
beliefs. We're in the Holy Week, if you're Christian, now is the time to
take a break from work and start focusing on living the days ahead to
the fullest. Regardless of your beliefs, I would still ask you to
treasure rest, and to use it with purposed. I also ask you to have
periods during the year that are different from each other, where you
focus on different things, and that give you and yours some sense of
rhythm.



## Rest is No Longer Sacred

I know that as Catholic I'm not a neutral observer, especially when it
comes to religion and secularization. I don't wish to come across as
tone deaf, let alone do I want to offend people with other beliefs. With
these disclaimers out of the way, allow me to question if for
a secularized society obsessed with "productivity" and "success" rest is
no longer something to be treasured. In Judaism and Christianity, rest
is sacred, Jews observe the Sabbath and Catholics observe Sunday and
holy days of obligation. Truth be told, Jews and Christians do more than
just rest on these days---they devote them to prayer. Still, the fact
remains that these are days where they don't work if possible. When
resting on a given day is no longer sacred, employees' days off work are
no longer aligned, and are often shifting. Moreover, hustle culture
probably pressures us to work on those days off to make some extra
money, or advance our career. A lot can be said, and has been said by
people more knowledgeable than I, about what having no rest does to work
life balance and to our mental health. In this post, I'd prefer to focus
on what this lack of rhythm, more precisely of collective rhythm, does
to our life.



## Do We Have Rhythm?

As I noted, "when resting on a given day is no longer sacred, employees'
days off work are no longer aligned, and are often shifting.". I reckon
that as a society this makes us loose our sense of rhythm. You could say
we have non-religious national holidays (note that even the term holiday
comes from holy day), with the potential to be a day of rest for
everyone, regardless of their beliefs. However, I'm afraid our
obsession for "productivity", "success", and hustle culture, make it
hard for that potential to be achieved. More and more people are asked
to work on those days. It is no longer just doctors or police officers
that must always be on call. I know that at least Judaism, Christianity,
and Islam, not only have holidays as they have periods of time devoted
to more intense prayer, fasting, and charity, as well as feasts lasting
for several days. With this periods occurring roughly around the same
time each year, there is a rhythm to the way each year passes (many
periods are movable as to the day and month they fall on each civil
year, but they are still predictable). You can say that many western
societies still celebrate Christmas and Easter, but not only do they
ascribe secularized meanings to them, as they distort their timeframes.
For instance, people start to have office Christmas parties before
Christmas, when Christians are still on Advent, a time of penance in
preparation for Christmas. Then, for Catholics Christmas day is
celebrated, liturgically (in Masses), for eight days. Christmas itself
lasts until the Epiphany (i.e., *Dia dos Reis*, *Dia del Reyes*). Easter
is preceded by Lent, a period of 40 days, but Easter Sunday is also
liturgically celebrated for eight days, and Easter time lasts until
Pentecost, which is 50 days after Easter Sunday. Psychologically,
I reckon this gives you a sense of rhythm for the year, it makes days
more different from each other. We all saw during the pandemic how our
routines gave us a sense of time, and how lost we feel without them.



## Take a Break, Follow the Rhythm

I'm writing this during the Holy Week, if you're Christian, now would be
a good time to take a break from work and start focusing on living to
the fullest the days ahead. Regardless of your beliefs, I would ask you
to take rest as something to be treasured and used with purposed. I also
ask you to consider the importance of having periods during the year
that are different from each other, where you focus on different things,
and that give you and yours some sense of rhythm.



## Thank you

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
