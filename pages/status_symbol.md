# Stop Making R a Status Symbol

**Date: 2023-04-27**



## Summary

I speculate that using `R` may have become a status symbol, when it
should be no such thing. Moreover, I suggest that, counter-intuitively,
dropping the high status may actually help to increase `R` adoption.


## What I've Noticed

Maybe it's just me but I've noticed people assume that if someone "knows
`R`" they must be a statistics genius. People also seem to believe,
perhaps unconsciously, that using `R` is somehow morally superior to
using `SPSS`, `JASP`, `Jamovi`, or other softwares with a graphical
interface. I'm biased, but I can concede that people who use `R` may
on average know a little more about statistics than people who use
`SPSS`. Still, correlation doesn't imply causation. If there is such
correlation it's probably due to the contextual factors. For instance,
people teach `R` mostly in advanced courses, whereas they teach `SPSS`
in intro courses. Moreover, the people who learn `R` on their own may be
more motivated to learn statistics, or in need of more advanced
analysis, than those who stuck with the software they were taught first.
Regardless, the point is that there is nothing about using `R` that
automatically makes someone an expert in stats or programming. Likewise,
there's nothing about using `SPSS` that automatically makes
a statistical expert forget everything she knows about statistics, or
an elite computer hacker forget what she knows about programming.



## Using R != Knowing R

One reason why using `R` shouldn't be a status symbol, is that in
reality a lot people who use `R`, for data analysis, don't actually know
the language that well. If you "learn" `R` just to get an analysis done,
you're probably "learning" little asides from copy pasting. No judgment.
That's how we all get started with programming, we change something in
a script, and see if the program still runs or if it breaks. Moreover,
if you're just focused on getting some work done you're may not have the
time or the patience to actually try and learn the language. Again, no
judgment. `R` is a tool, so it might as well be useful. Still, this
means that using `R`, even for a complex analysis, is not really the
same as really knowing `R` or programming.



## Knowing R != Knowing Stats

I've noted that people who use `R` may not actually know the language
that well. I should also note that one can know the `R` language, and be
proficient in it without knowing that much about statistics. Maybe the
former is more common in psychology, since people are drawn to `R` once
they want to learn more about statistics. Still, the latter can also be
true. In fact, that was my case. I didn't knew that much about
statistics when I started to learn `R`. I actually wanted to use `R` as
to relearn statistics. As I'm geek, and I had started to learn
programming in `Python` (to be clear I had taken the first steps,
I hadn't studied computer science topics at the time), learning the
language wasn't that hard, but relearning statistics was. Regardless,
this is all to show that using or knowing `R` does little on its own to
improve your understanding of statistics.



## Will Dropping the High-Status Hinder Adoption?

It might, but I think it won't. In fact it might actually help adoption
if `R` is seen as something easier to learn. What could help adoption
even more is if `R` is perceived as the norm. For `R` to become the norm
it will have to loose some of its elite status. This leads us to the,
perhaps counter-intuitive, conclusion that if you want to increase
`R` adoption you should strive to make it less of a mysterious elite
skill, and more like an approachable tool to use every day. In academia,
unfortunately, we already have enough reasons to suffer from impostor
syndrome, let's not make `R` another reason for people to feel bad about
themselves. Let's make `R` accessible for everyone!


### P.S:

If you want to learn `R` and/or help making `R` more accessible consider
[signing up for RUGGED](https://rggd.gitlab.io/onboarding.html), and
contributing to [our projects](https://rggd.gitlab.io/projects.html).



## Thank you

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
