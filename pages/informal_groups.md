# Informal Groups of Colleagues

**Date: 2023-02-11**


## Summary

I rant for way too long on the benefits of informal groups of colleagues
(broadly defined). I urge you to join or create a group if you're
a junior, and to encourage their formation, if you're a senior.
I provide some examples of groups I've attended and one that I'm still
attending, concluding with some tips and tricks for managing informal
groups.


## Definition

When I refer to informal groups of colleagues I'm just referring to
groups of colleagues that meet without any formal obligation or
oversight. The group's formation could have been encouraged by a senior,
but that senior is not present in the meetings. Also, I'm not being
strict about who counts as colleague or who doesn't. If you're in
a group of colleagues, and you're meeting without any formal obligation
(e.g., attending the meetings is not part of your job description), then
most of what I discuss in this post should apply.



## The Case for Informal Groups

Getting a PhD can feel pretty lonely at times. At my department the last
year of your masters can also feel lonely since you start an internship,
as well as a thesis project, and you stop having your schedule filled
with classes that you attend with your fellow students. Being the new
person at a lab/company/organization feels daunting. Having colleagues
to share that experience with makes it all the much more bearable.
Still, if the nature of the job leaves you working mostly independently
from each other, and are always surrounded by seniors/managers, you end
up feeling like it's hard finding the time to share, vent, and relax.
Importantly, and I would like you to seriously give this some thought,
the seniors/managers may be much more approachable, empathetic, and down
to earth than you think. I've seen many people feel a lot of distance
from a senior that seriously cares for them, and wants to be a good
close mentor. Still, I know some war stories, from people working in
overly competitive environments, or in places where seniors are tough on
juniors (even when it comes from a good place). Such situations usually
give rise to the all too prevalent [impostor
syndrome](https://en.wikipedia.org/wiki/Impostor_syndrome). The only way
to defeat the impostor syndrome is to share your feelings, defeating
[pluralistic
ignorance](https://en.wikipedia.org/wiki/Pluralistic_ignorance) (shout
out to [André Mata](https://orcid.org/0000-0001-5087-4919) for
explaining that to me). Even that sometimes is not enough. You get the
impression others feel like impostors when they're not, and you're
still the only real impostor there. Trust me, you're wrong, you're not
an impostor. All this to say, informal groups of colleagues can be great
for your mental health, and they give you a network of people that you
can rely on for getting answers for personal and professional questions
alike.



## Personal Case Studies


### LiSP Student Meetings

My PhD program---LiSP (Lisbon PhD in Social Psychology)---is a joint
collaboration between the several institutions in Lisbon that offer PhDs
in social psychology. This means the program got applicants (it is no
longer open to applications) from all the institutions involved, and we
got to attend classes on all institutions, during our first year. After
our first year though, we start working separately from each other,
within our own institution. Moreover, depending on the institution, we
may be part of a lab that meets more or less regularly, with more or
fewer fellow colleagues. This lead students to feel more isolated after
the first year, and made the case for having student lab meetings much
stronger. When I got on to the program I believed they had already had
active meetings, but I also recall their momentum slowing down. I don't
recall exactly when now, but we started to try and get the meetings to
happen more frequently and have greater attendance. I'll be honest,
attendance was always an issue, but we did get the meetings to be more
regularly, specially when we decided we wouldn't cancel a lab meeting
even if no one had volunteered to present. The meetings also became more
and more informal, giving us a place to share and be social. I don't
think I'll ever forget how they helped me stay sane during the pandemic.


### WriteOn Workshop

Back in my department, my advisor, [Sara
Hagá](https://orcid.org/0000-0003-4957-4702) started to host the
[WriteOn Workshop](https://osf.io/n8pc3), a writing workshop developed
by [Barbara Sarnecka](https://orcid.org/0000-0003-2432-3712). What made
this writing workshop so different is that it didn't have any lectures.
The workshop was also not a one off event, in our case it was
a year-long class. In each session, we had quiet writing time, we had
a feedback forum, and we commented on Barbara's blog posts/book
chapters. To be clear the format was Barbara's brainchild, but I think
Sara's execution was great, as well as how she managed to adapt it to
our department's constraints/needs (e.g., the sessions had to be
shorter). This writing workshop doesn't fit the definition of informal
groups colleagues I gave at the beginning. It doesn't fit it yet...
Barbara's plan included the possibility that students would continue
meeting informally, after they had participated in a rendition of the
workshop. Sara encouraged us to do the same and we ended up forming
a group that continued to meet, following more or less (more often less
than more) a similar session structure. Many thanks to all the
colleagues that accompanied me in that group, for putting up with me,
for all the good they did to my mental health, and for the quality
writing feedback they provided.



### RUGGED

When I was participating in WriteOn, I was reminded of an `R` summer
school, lectured by [Marcelo
Camerlo](https://orcid.org/0000-0002-0716-189X) I had attended over at
[ICS](https://www.ics.ulisboa.pt/). If I recall correctly, Marcelo had
suggested we set up an `R` user group where we could share code, tips,
and tricks about using `R` for data analysis. To the best of my
knowledge, that never materialized, but the idea seems to have stuck in
my mind. I remember thinking how WriteOn's session structure could be
adapted for an `R` user group. We could have quiet ~~writing~~coding
time, we could discuss blog posts/resources on `R`, and we could give
feedback on each other's ~~writing~~code. I discuss this idea with
[Magda Sofia Roberto](https://orcid.org/0000-0003-4127-561X), and with
former LiSPer [Cristina
Mendonça](https://orcid.org/0000-0002-1814-5270), when we were planning
several training initiatives on data analysis, for our department.
I ended up giving a workshop on linear mixed models over at our research
center---[CICPSI](https://www.psicologia.ulisboa.pt/cicpsi/). I pitched
the idea of forming an `R` user group, and thanks to all the people that
joined, [RUGGED](https://rggd.gitlab.io) was born (stay tuned as I'll
probably write even more about [RUGGED](https://rggd.gitlab.io) in the
future). Seeing [RUGGED](https://rggd.gitlab.io) grow, and seeing my
colleagues contributing to [RUGGED
projects](https://rggd.gitlab.io/projects.html) has been amazing! I look
forward to our meetings and to seeing [our
projects](https://rggd.gitlab.io/projects.html) starting to take shape.
If this sounds like the sort of group you'd like to be part of, take
a look at [RUGGED's onboarding
page](https://rggd.gitlab.io/onboarding.html).



## Join/Create Encourage Informal Groups

I hope this has been enough to persuade you to at least consider joining
or creating such a group. Take your time to think, but don't hesitate
reaching out to your colleagues to if they want to set up such a group.
If you know of groups that exist already, or feel like joining
[RUGGED](https://rggd.gitlab.io), go for it! In case you're a senior...
well you can always set up a group with other seniors...but you can also
encourage the juniors that you mentor to form such a group. Suggesting
that they form a group will show them you care for them, and you're not
trying to divide and conquer. It will signal that you understand what
they're going through, and that you're the type of person they can reach
out to. If you do it right it, their group will not serve to keep them
from engaging with you and with other seniors, but as place where older
colleagues tell newcomers the seniors in the group are open and nice to
juniors. I mean, I can't guarantee it will always work out that way, nor
that nasty things like bullying won't happen in the group... Still, bad
things can always happen, we can only work to build good ones and
incentivize others to do the same. Anyway... If you're a senior, I would
also like to encourage you to say that you're free to come to one of
their meetings should they ever wish to speak to a more experienced
person, about what's like working in the field. If you're junior,
I would suggest inviting seniors over to one or more meetings about
career development.



## Tips and Tricks

Now that I've made my case, and shared my experiences let me just give
you some tips and tricks on how to manage informal groups before I sign
off. I see myself as severely lacking in social skills (other than
public speaking which I weirdly enjoy), so I would take my advice on how
to build and manage informal groups with a large grain of salt. Still,
I would like to share with you what has worked well for the groups
I participate/d in.


### Don't Skip Meetings

In my experience, skipping meetings leads to an erratic schedule, and
keeps people from setting a recurrent event in their schedule, and in
their memory. This is particularly problematic for lab meetings or
meetings where there's a rotation of who is speaking that week. As soon
as you start canceling meetings because there's no volunteers to present
you run the risk of having an erratic meeting schedule. Moreover, you
risk creating the incentive for people not to volunteer just so they can
get out of another meeting. If you have an informal group, that aims to
be an environment for sharing and letting off some steam, not meeting
because no one volunteered to present sends the message that you're all
business (since you're not meeting when there's no business to discuss).


### Consider Only Meeting Biweekly

When you feel like meeting every week is to much of a burden on
everyone's schedule, you consider meeting biweekly ([as in once every
two weeks not twice
a week](https://www.merriam-webster.com/dictionary/bimonthly)). This
gives you a better shot at not skipping meetings. I feel like meeting
less than twice a month runs the risk of being harder for people to
track of when you're meeting, and to setup a recurrent event in their
memory and schedule. If you're meeting biweekly, my advice is that if
you ever skip a meeting (which I advise against) you refrain from
anticipating the next one, as that means reconfiguring the recurring
event on people's schedules.


### Keep it Short

I confess I suck at following this advice, as I'm usually the reason the
meeting gets prolonged after everyone already wants to leave. Do as
I say, not as I do I guess. Meaning, try to keep the meetings short, not
too short so that people don't get a chance to share, just try to leave
people wanting more, instead of feeling like the meeting went on for too
long. When people feel like the meetings extend for far too long they
are less likely to come, particularly if they feel busy. This is also
a cultural thing as we Portuguese have the habit of long saying very
goodbyes, and continue to bring up new topics after we've said goodbye
ten times already (I'm very guilty of this myself).


### Manage Your Expectations

If you're the one who started the group, or if you're spearheading its
logistics it is easy to feel like other people are not as involved as
you are. I know that can feel sad, but I advise you to try and make your
peace with that. Maybe, if you think about it, you're less involved in
other groups than other people there. Regardless, if you want the group
to be informal, showing you want them to contribute, but you're not
pressuring them to, makes them feel valuable but not pressured. Again,
I'm probably guilty of pressuring people to contribute and participate
more than they want to, but I am trying to change that.



## Thank you

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
