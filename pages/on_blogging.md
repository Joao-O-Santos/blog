# Meta-Post: Writing and Blogging

**Date: 2023-03-18**


## Summary

I share my difficult relation with writing, and the pivot points in that
relation, namely, participating in the WriteOn workshop and starting
this blog. I make the case for why blogging can feel so liberating, and
different from social media, encouraging you to start your own blog. If
you do start a blog, I'd be honored if you'd share it with me. Still,
the thing about blogging is that you don't even have to share it with
anyone.


## Writing and I

### Handwriting Made Me Hate Writing

Few people know this, but when I was little I had issues with fine-motor
control and my handwriting sucked. As handwriting was how I wrote at the
time, and (hand)writing took place on such joyful occasions as doing
homework, or answering test questions, I can say I wasn't a big fan of
writing. This was compounded by the fact that I felt like my writing
ability was far worse than my ability to speak in any setting, including
the classroom. This trend then continued throughout my life.

### The Computer Helped But Not Much

I don't recall exactly when, but the first major welcomed-change likely
was the ability to submit computer-written assignments, regardless a lot
of handwriting still took place... I continued to define myself as
a speaker not a writer.

### Realizing that I Was Going to Be a Writer

The second major change, at least the one I can recall, in my difficult
relationship with writing came much later during my (still ongoing) PhD.
My advisor, [Sara Hagá](https://orcid.org/0000-0003-4957-4702) started
to host the [WriteOn Workshop](https://osf.io/n8pc3), a writing workshop
developed by [Barbara Sarnecka](https://orcid.org/0000-0003-2432-3712).
I've written about that workshop, what it meant for me, and how it
inspired [RUGGED](https://rggd.gitlab.io), in [a previous
post](./informal_groups.html). Thus, I won't go into much detail about
the WriteOn workshop here. Instead, I'll focus on how it changed my
relationship with writing. The debate and discussion in the first
sessions of the WriteOn workshop made come to the powerful realization
that, whether I liked it or not, in most jobs you end up being a writer.
This is abundantly clear in academia, particularly in [an academia
obsessed with counting publications](./alma_mater.html), but I realized
it was true for many other jobs. As long as you have to write emails,
reports, fill out forms, submit proposals, you end up writing. Realizing
writing was likely to be a big part of my job, even if I left academia,
didn't make me a better writer. Still, I feel (and hope) participating
in the WriteOn workshop, learning from the feedback of my colleagues,
and that of my advisor even outside the workshop, made me a better
writer.


### Blogging

#### Blogging vs. Scientific Writing

The third major change in my not-so-difficult-now relationship with
writing came with this blog. I created it out of a whim. The impact it
had on me was greater than I imagined. WriteOn taught me a lot about
writing, particularly but not only, about scientific writing. It taught
me that scientific writing doesn't have to strive for unintelligibility
of its message, or for maximizing the cognitive-strain of its readers.
WriteOn taught me to rewrite sentences like the previous into something
like: scientific writing doesn't have to be dull, long-winded, or hard
to understand. It also taught me to revise that into something more
positive: WriteOn taught me that scientific writing can be sharp and
engaging. Barbara Sarnecka would probably suggest to avoid using two
adjectives. This might leave me saying that: WriteOn taught me that
scientific writing can captivate the reader. As scientific writers we
should avoid being pretentious and strive for clarity. But I digress...
I just wanted to say that scientific writing doesn't have to be dry, it
can be engaging, but it still has a lot of rules (just count the pages
in APA 7th style manual). Besides the style rules you also have to do
your due diligence as a scholar, you have to cite your sources, ensure
you're painting an accurate picture of the literature, triple-check your
analysis, discuss competing hypothesis/explanations, etc... This means
scientific writing ends up being demanding, and often feels restrictive.
Sometimes you just want to vent something, or you have an idea you want
to share without having to write an entire paper about it. That's when
I find blogging can be a incredibly liberating experience (sorry for the
double adjectives). Your blog can be your place to share your feelings,
present your new ideas, or share someone else's ideas that you find
captivating.

#### Blogging vs. Social Media

If this blog had an audience, other than my dad and that one person who
makes the mistake of clicking on a link in my LinkedIn post, I could
point out that the youngest in my audience would say social media
can do all I said a blog could do. I would suggest that blogging still
feels differently. I know this feels like I'm just out of touch with
the world today, I know I am, but I think that's not the case here, hear
(read) me out. When you post something on a social network it's there,
it's on that network, it may appear into your friends' feeds, but it
will appear next to any number of posts. Contrast that with your blog
that features only your posts. Posting on your blog also means your post
is just yours, you have all the control over it, and not part of
someone else's network. I'm afraid this doesn't seem like an issue
for most, so I'll leave a discussion of our digital rights for later...
Anyway, back to blogging vs. social media. Another major difference
between blogging and social media is the length of posts. In social
media you're expected to make very short posts, or maybe apologize when
you write a so called "wall of text". In blogging your posts can be as
short or as long as you wish, whether or not people will read them is
another issue. Speaking of readers, social media is about engaging with
an audience as a creator, and engaging with the creators and fellow
audience members as a consumer. You can usually tell how many people saw
your post, you see and reply to their comments, etc... Blogging doesn't
need to allow for any user engagement. You can write for yourself or for
an audience, but you don't have to obsess over how much they engage with
it. If you want you can try and add comment sections and other forms of
user engagement, but that's up to you, and you usually have a greater
control over of how those interactions take place. I believe this also
contributes to blogging feeling so liberating.


### What Hasn't Changed

I still don't see myself as an amazing or prolific writer. I'm remain
convinced I'm a better speaker than a writer. But I began to take
pleasure in using writing to share my thoughts, and feelings. WriteOn
had already taught me that writing can help clarify thinking, but as
I mentioned if my only writing was scientific writing all thinking
processes would be quite demanding. Blogging has helped me clarify other
thoughts. Maybe one day I can try turning a few into scientific papers,
but if that day comes, I'll know they are already published on my blog,
despite whatever reviews I get from a publisher.



## Blogging and You

If anyone is reading this is the part where I try to motivate you to
write a blog. I don't have more points to make in favor of blogging.
Just think that if you find other forms of writing tiring, or if you
find social media too restrictive or too stressful, start a blog. You
don't have to tell anyone... You may not even be able to tell how many
people read it... But you'll get to write in it! And maybe, just maybe,
you'll find it liberating! So, why don't you give it a try?

If you do try out blogging, I would feel honored if you would share it
with me. It's blogging, you don't have to share it with anyone, but you
can share it with whomever you choose.



## Thank you

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
