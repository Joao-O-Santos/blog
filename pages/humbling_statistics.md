# Statistics Make Everyone Humble

**Date: 2023-02-20**




## Summary

Have you ever noticed how people frequently report struggling with
statistics, but don't usually do the same regarding other (arguably)
equally difficult subjects? I describe what I've seen, and provide some
possible explanations for this phenomena. I'm really interested in
knowing what are your thoughts on this, so please open an issue in [this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues), or
reach [me in LinkedIn](https://linkedin.com/in/joao-o-santos) with your
ideas.



## What I've Noticed

Everyone claims to struggle with statistics and to be ignorant about
them. To be clear, statistics, can be a complex field, and navigating
the lively debates in the field (e.g., frequentism vs bayesianism) is
hard. Still, from my experience people complain about struggling with
statistics much more than they complain about understanding their
research topics. To my naked eye their fields of study don't seem that
much easier, or simpler, than statistics. Maybe this is just be being
a victim of [the curse of
knowledge](https://en.wikipedia.org/wiki/The_curse_of_knowledge), as
I like statistics, but I think it is more than that. It's also worth
noting that this seems like an instance of the [below average
effect](https://en.wikipedia.org/wiki/Worse-than-average_effect), the
opposite of the, better-known, [better than average
effect](https://en.wikipedia.org/wiki/Illusory_superiority). Still, that
is not an explanation for why this occurs, just a suggestion for how to
label the effect.



## [Intermission: It's Not Statistics](./its_not_statistics.html)

I ranted about why I reckon most people are struggling with something
else, when they claim to be having a hard time with statistics, in
a [previous post](./its_not_statistics.html). Still, even if you agree
with my points, that just means people blame statistics for other
problems they're having, it doesn't account for why they blame
statistics, and not some other innocent topic.



## Possible Explanations?

Let us try to come up with some possible explanations for this phenomena
then. By *us*, I do mean *us*, I'm going to put forward some hypotheses
but I would like to see what you think. Feel free to open an issue in
[this blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues), or
reach [me in LinkedIn](https://linkedin.com/in/joao-o-santos).


### Tradition

This uninteresting hypothesis, posits that people started to report
struggling with statistics, and that made others comfortable sharing the
same. Since the same didn't happen for other areas of expertise,
statistics remain the only area about which people report doing that.
This hypothesis as little explanatory power given it doesn't account for
why people started reporting struggling with statistics, specifically,
but not other areas.


### Being Afraid to Look Arrogant

This isn't really an explanation as to why people only report
struggling with statistics. Instead, it's merely an hypothesis about why
the effect persists. This hypothesis states that the effect persists
because it is hard for people the people who actually like statistics,
and feel comfortable working with them, to come out and say that.


### Lack of Training

According to this hypothesis, people actually struggle with statistics
because university curricula provide insufficient training in this area
(but not on others). I do think this is true, in most cases, but I also
think there is more to it than that.


### Statistics are Math

Statistics can be considered a field of math (though I recall my
high-school math teacher saying that was up for debate). Math is already
victim to many prejudices, with a reputation for being harder than the
other classes. Maybe people are simply perceiving statistics as math,
and being equally prejudiced towards it.


### It's a Practical Skill

Unlike many other areas of expertise, statistics, in what concerns
psychological sciences and related fields, is a practical skill. Not
only must we understand the underlying theory, as we must know how to
operationalize and test our hypothesis statistically. Doing data
analysis, and having a hard time with it (e.g., not being able to
perform an analysis in a given software) may provide people with more
feedback, than when people are writing a literature review. Statistical
software gives out errors, but word processing software won't
complain if miss important points, or misinterprets some key findings,
in your literature review.


### All the Above?

If we think about it the above explanations are not really mutually
exclusive, so all of the above can be true (or partially true), or some
combination of them at least.


### What Do You Think?

I'm interested to know what you think, please open an issue in [this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues), or
reach [me in LinkedIn](https://linkedin.com/in/joao-o-santos), with your
ideas.



## Thank you

**Thank you so much for reading!**
