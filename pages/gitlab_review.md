# How a Suckless Fan Moved to GitLab

**Date: 2023-02-23**

**Edited: 2023-02-24; 2023-02-25; 2023-03-06**


## Summary

I rant about my journey moving from only using `git` locally, to having
almost all my projects up on [GitLab](https://gitlab.com). If you
haven't given GitLab a try, consider taking it for a spin.



## The Suckless Philosophy

I don't know exactly when, but during my `Linux` journey, I stumbled
onto [suckless](https://suckless.org). In no way am I affiliated with
the project, nor am I current on what its members have been up to.
Still, in general, I do find [their
philosophy](https://suckless.org/philosophy/) quite interesting and
inspiring. This is not to say I agree with them 100%, nor that I always
follow their approach in my projects. I do encourage you to read [said
philosophy](https://suckless.org/philosophy/), and to checkout their
projects. The gist of suckless' reasoning is that the more features you
write into your software, the more you run the risk of adding bugs.
Since people seem to value features and complexity over simplicity,
complexity grows exponentially, while quality decreases. Minimalist
`Linux` users like me, tend to agree with them. We've learned to love
the `UNIX` philosophy of having programs do one thing, and doing it
well. We've learned to leverage several small programs to perform more
complex tasks, rather than using a complex program to perform several
small tasks.



## GitLab

Before I list what I personally viewed as GitLab's pros and cons, let me
just clarify right away that this isn't a direct comparison with any of
its competitors. I do make some comparisons with GitHub, but I also
recognize that many of the pros and cons I list apply to both. Note also
that my intention isn't to provide an objective review, I'm just trying
to reconstruct my thought process, and what made me rethink my approach.


### Cons...So Many Cons...

#### GitLab's Sales Pitch

On the surface GitLab seems to be the opposite of the suckless
philosophy. It has tons of different features, and it positions itself
as the "one DevOps platform". Contrast this with GitHub which encourages
you to leverage several external industry standard tools. Note that
having "tons of different features" seems like a con to me, not a pro.

#### Closed-Source Rings

GitLab is built on an open-core business model. Meaning, that its core
components are open-sourced, but some other tooling is not. I don't know
if software has rings like a tree or a planet, but if only the core is
open-sourced, the rings are closed-sourced, and you have to get through
the rings to reach the core.

#### It's a Cloud-Based SaaS

GitLab runs in the "cloud", meaning your data is stored on someone
else's computer. Moreover, it operates a software as a service business
model. As a privacy and open-source enthusiast this also seemed like
a con to me.

#### Graphical Web-UI

GitLab is a website, as such you access it through a web browser, and
interact with its web UI (user interface). You do have some alternative
UIs and mobile apps, but we'll get to that later. GitLab's web UI even
features functionality to allow to edit files online, and even includes
a Web IDE (integrated desktop environment). Moreover, GitLab allows you
to create and track issues, becoming a tool for project management. I've
professed [my love for the terminal before](./text_uis.html), so suffice
it to say having a graphical non-terminal based UI is also a con to me.
Being a minimalist `Linux` user, I see my whole OS as my IDE, and I want
to write/edit everything in `nvim`. Further, I wanted to keep track of
my TODOs with a `TODO.md` file. In an hobby project, I even wrote
a script to `grep` for TODO comments in my source code and (re)generate
my `TODO.md`. Thus knowing the suckless philosophy, seeing so many
features, which I didn't even plan on using, made it hard to see GitLab
as something other than bloatware.

#### It Hides `git`

When interacting with GitLab's web UI you don't actually have to
understand `git`, or know how to use it in the terminal, if you're just
using basic functionality. Again, for someone who gets so wound up when
people mistake GitHub for `git` (I understand why people do it though,
and I don't judge them), this again is a con not a pro.

#### GitLab's CI/CD Relies on Docker

I'm not a `Linux` system administrator, I just manage my own machines.
When I started with GitLab, I viewed `Docker` (and containers in
general) as a solution to a problem that should never have existed. To
me containers existed to solve the dependency hell introduced by
languages like python.

Relevant xkcds are relevant:

![containers](https://imgs.xkcd.com/comics/containers.png)

![python](https://imgs.xkcd.com/comics/python_environment.png)


### Pros

#### Only Learning One Tool

Even though there are many CI/CD tools out there, they are not exactly
small, so it is not like they are super appealing to a minimalist
`Linux` user. Deciding which tool to use, and figuring how to use it in
conjunction with the code forge you use can seem daunting to beginners.
Moreover, if I was to use some other competitor I would have to learn
how to configure it to use the external tools I chose, or manage those
tools independently of the code forge. My solution to this was trying to
create some DIY solution using shell scripts, but that wouldn't really
accomplish all I wanted to try without having to create VMs (virtual
machines) or containers. Having those tools on my system would only make
my system less minimalist. I figured that at least with GitLab I only
have to learn one tool, and a tool that I already was using since I had
to host my `git` repos somewhere.

#### It's Open Core and Makes Money

An open core business model is still not my favorite. Yet, a look at the
fully open source alternatives shows they are either self-hosted or they
are (justly) paid. [Codeberg](https://codeberg.org) the fully open
source alternative that interested me the most, does have a free tier at
the moment, but we can expect it to be removed in the future.
Electricity costs money so I understand why fully open source
alternatives with small user bases, and without mountains of funding,
have to charge. Even if they didn't I would feel like I was freeriding
on their system, since right now it's hard for me to take on more fixed
expenses. GitLab on the other hand, seemed to be doing good financially,
their user base seemed large, and a focus on enterprises meant their
business rested on getting those enterprises to buy many licenses,
rather than individual developers. Offering a free tier is probably
worth it to get more developer mind-share, as developers who enjoy
GitLab for personal use might convince their employers to buy it for the
company. I for instance am trying to convince more people to join
GitLab. This might mean I'm just onboarding more non-paying users, but
maybe if one of them subscribes to a paid plan it will make up for it.
I must admit however that the recent wave of layoffs has made me sad,
and less optimistic about GitLab's finances.

#### Remotes Are Useful

GitLab is indeed a cloud based SaaS, but you can still self-host its
open source version. Moreover, as I learned, when you start working on
more than one machine, having a remote starts becoming a necessity.
I also never had any reason to suspect GitLab would do something fishy
with my data. For ethical reasons I do try and keep participant
personally identifiable data, from my studies, away from it. To be
honest GitLab probably would take as good care of that data as the
software used to collect it, or as the email providers that researchers
use to sen unencrypted datasets to each other...

#### You Have `glab` Let the Others Keep Their Web-UI

I recently discovered `glab` a command line tool that helps you interact
with GitLab. It doesn't offer all the features of the web-UI just yet,
but it offers plenty, including the most frequently used ones. Besides,
GitLab, like GitHub and other competitors, does nothing to stop you from
interacting with your `git` repos using plain `git`, and editing your
files with your text editor (running on your operating system of
choice). This means that I can continue to work on my files as I always
have, I can `git push`, and I can even manage issues, all without
leaving my terminal. GitHub also offers a command line utility so `glab`
isn't really a differentiating factor, but it does allow me to use
GitLab without having to leave the terminal.

*Note: bonus points for anyone who spots the Song of Ice and Fire
reference in this section's heading.*

#### Separates Project Management From Code

I was used to keeping my TODOs and project management files locally.
I would keep each project's TODOs in its repo, and manage which projects
I was going to be working on in a separate file. The thing is if you
start thinking of working on multiple computers, working with multiple
people, and/or having branches, separating project management from code
begins to seem like a requirement.

#### GitLab Doesn't Pretend to Be `git`

I have seen how people mistake GitHub for `git`, and I suspect that
GitHub doesn't mind that very much... I was very pleasantly surprised
when I saw that GitLab's documentation clearly shows how to interact
with `git` in the command line, and doesn't push any proprietary app to
replace working with `git`.

#### It's All Just Linux Anyway

I still think `Docker` is often use to fix problems that should never
have existed in the first place. However, I have come to accept it can
be quite useful for testing your software in an isolated environment,
and to deploy it in a scalable way with `Kubernetes` (but I haven't
played around with that last one yet). When you make sure your `Docker`
images only include the required dependencies and nothing else, you end
up with an even more minimalist machine than yours. Moreover, if your
CI/CD pipeline fails because you're missing dependencies, you learn that
you must have missed some dependency. Further, if you use a different
`Linux` distribution from the one you daily drive, you have a way to
test for portability. I for one enjoy building images based on the
`alpine` `Linux` distribution, which is built on `musl` not `glibc`, if
your software compiles on alpine you have some indication of portability
and minimalism. Having GitLab's CI/CD testing my software in their
cloud (i.e., their computers), means I don't even have to have `Docker`,
and other tools installed on my machine. Note that I do take some steps
to avoid vendor lock-in. My CI/CD jobs aren't that complex, so it
shouldn't be hard to port them to another vendor should the need arise.
I also really heavily on `(GNU) Make`, I try to get a recipe for pretty
much every CI/CD job. This way I can test things on my machine and the
`script` portion of my CI/CD jobs is kept to one or two lines of code
shell script, a lot of times it's just reads `make something`. That's
also what surprised me the most with `Dockerfile`s and `.gitlab-ci.yml`,
they are all pretty much just a way to run `shell` scripts, in a machine
configured to your exact specifications.

#### Namespaces

A really cool feature about GitLab is how easily you can create groups,
subgroups. Again, other competitors may have similar features, but from
my experience GitLab makes it really easy, and allows for tremendous
flexibility in setting up groups and subgroups. This basically adds
another level of organization allowing to setup a tree like organization
for managing your repositories and your multi-repository projects.

#### GitLab Pages

I find GitLab Pages to be an incredibly useful feature. I know GitHub
has something similar, but GitLab Pages are less opinionated about which
static website you use. Moreover, since you can set up groups and
subgroups so easily, and you can have a main page for each, it is easy
setting up websites for the projects you're working on.




## GitLab and I

### Past

For the longest time I pretty much just used `git` locally. I didn't set
up remotes, and I had only one branch. Still, I collaborated with my
colleagues on my PhD program's website, I needed use GitHub. I really
didn't like having to open the web browser to go on GitHub's website.
I know they have a `command line` utility---`gh`---but I probably didn't
know that at the time. When I started working on QHELP (a project for
teaching quantitative thinking, funded by Erasmus+) student projects,
I finally had to rely heavily on branching. I also grew a bit
frustrated with GitHub for how it imposed a particular workflow on my
colleagues with its app. Given most of them were using the GitHub there
was also a "language barrier" because I couldn't really teach `git`,
since they were not interacting directly with it. Probably there's a way
to interact more directly with `git`, withing GitHub's app, but I lacked
the time or the inclination to learn a proprietary tool that was giving
me headaches.


### Present

After dreaming about it for ages, I ended up finally building my own
desktop. It it's a simple machine, it's not meant for gaming nor
anything crazy, but it's more than powerful enough for my needs. My
living arrangements changed quite a bit I found myself working both on
my desktop and on my laptop. After a few days of saving work to flash
drives or emailing stuff back and forth, I started doing everything
I could to set up remotes for every project, and to start tracking
with `git` the few projects I wasn't doing that for. I don't recall
exactly when but I decided to give GitLab CI/CD a try, which then lead
me to learn about how to setup basic `Dockerfile`s. From that moment on
I was hooked. That as the inspiration for
[SciOps](https://gitlab.com/rggd/SciOps) a project I'm spearheading,
over at our `R` user group---[RUGGED](https://rggd.gitlab.io). I'll
probably dedicate a post or more to SciOps, in the future, so stay
tuned. When I got the chance to speak about SciOps on a workshop,
I asked GitLab for help on getting resources and references. Since
I was, and still am, on GitLab's free tier, understandably but
unfortunately, the tech support people didn't help me much with my
issue. However, when I posted on the forums, [Philip
Metz](https://twitter.com/MetzinAround), a former GitLab employee, was
incredibly helpful and kind, providing more and better resources than
I could ask for. Having to work on two machines also increased the need
for a better task organization system. I may dedicate a full blog post
to that in the future... For now I'll just say that [GitLab's issue
boards](https://docs.gitlab.com/ee/user/project/issue_board.html) are an
amazing feature, and that `glab` already allows me to manage each
project's issues, and view the boards. On another note, the ease with
which you can deploy static websites with GitLab Pages was the driver
for developing [make
it\_stop](https://gitlab.com/joao-o-santos/make-it-stop)---my static
website generator. In fact, this blog and my personal website are all
deployed as GitLab Pages and compiled with `make it_stop`.


### Future

I obviously don't know what my future holds but I expect to continue to
use GitLab. It would be great if I could work for more clients willing
to use it (I've been fortunate enough to tutor two people willing to use
it). I'd also love to have enough contract work to justify buying a paid
subscription. If SciOps grows into something useful, I might even get
some academics to start using GitLab. Until then, I'll continue to play
around with GitLab's CI/CD, as well as its project management features.



## Give GitLab a Try

If you never used a code forge, consider starting with GitLab. If you
have used a competitor consider taking GitLab for a spin to see how it
compares. If this also seems like weird tecnho babble to you, but you're
interested to checkout its potential, consider contributing to
[SciOps](https://gitlab.com/rggd/SciOps) as an easy way to start.



## Thank you

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
