<!-- 1 MENU_ENTRY=Posts -->
# Posts



## [Stop Making R a Status Symbol](./status_symbol.html)

**Date: 2023-04-27**

I speculate that using `R` may have become a status symbol, when it
should be no such thing. Moreover, I suggest that, counter-intuitively,
dropping the high status may actually help to increase `R` adoption.

[Read full post](./status_symbol.html)



## [Rhythm and Rest](./rhythm_rest.html)

**Date: 2023-04-05**

In this post, I reflect on the consequences of no longer having rest as
something sacred, and no longer observing periods of time that give
a sense of rhythm to our year. I speak from a Catholic perspective, but
I hope the general sentiment can resonate with you, regardless of your
beliefs. We're in the Holy Week, if you're Christian, now is the time to
take a break from work and start focusing on living the days ahead to
the fullest. Regardless of your beliefs, I would still ask you to
treasure rest, and to use it with purposed. I also ask you to have
periods during the year that are different from each other, where you
focus on different things, and that give you and yours some sense of
rhythm.

[Read full post](./rhythm_rest.html)



## [Please Stop Paying to Teach SPSS](./ditch_spss.html)

**Date: 2023-03-24**

I make the case for why universities should not pay for bulk `SPSS`
licenses for students, and build statistics curricula around it. My main
points boil down to that constituting unpaid advertising, and creating
vendor-lockin, when there are plenty of better alternatives. Contrary to
popular expectations I will not argue for replacing `SPSS` with `R` on
all courses. Instead, I'll even suggest relying solely on interactive
simulations, other teaching aids, and teaching no software at all in
introductory stats courses. Ok... Those simulations can be built as
`R/Shiny` apps, but they don't have to...

[Read full post](./ditch_spss.html)



## [To Dad](./dad.html)

**Date: 2023-03-19**

Thank you dad for all you have done for me! Thank you so much for all
you have done with me! Heavenly Father, please help us all to be
a better image of You. St. Joseph, please, pray for us all, through your
intercession, and that of your wife, St. Mary, may God protect our
children, our families, and help us care for them.

[Read full post](./dad.html)



## [Meta-Post: Writing and Blogging](./on_blogging.html)

**Date: 2023-03-18**

I share my difficult relation with writing, and the pivot points in that
relation, namely, participating in the WriteOn workshop and starting
this blog. I make the case for why blogging can feel so liberating, and
different from social media, encouraging you to start your own blog. If
you do start a blog, I'd be honored if you'd share it with me. Still,
the thing about blogging is that you don't even have to share it with
anyone.

[Read full post](./on_blogging.html)



## [De Alma Mater ad Negligens Mater](./alma_mater.html)

**Pridie Kalendas Martias, Anno Domini MMXXIII**

I rant about how an obsession with counting scientific papers made
academia go from a nurturing mother to a negligent one. Still, due to
the tireless efforts of the many altruist academics, who care for their
students, academia remains a nurturing mother for her students. Consider
taking the time to thank the nurturing academics in your life.

[Read full post](./alma_mater.html)



## [How a Suckless Fan Moved to GitLab](./gitlab_review.html)

**Date: 2023-02-23**

I rant about my journey moving from only using `git` locally, to having
almost all my projects up on [GitLab](https://gitlab.com). If you
haven't given GitLab a try, consider taking it for a spin.

[Read full post](./gitlab_review.html)



## [Statistics Make Everyone Humble](./humbling_statistics.html)

**Date: 2023-02-20**

Have you ever noticed how people frequently report struggling with
statistics, but don't usually do the same regarding other (arguably)
equally difficult subjects? I describe what I've seen, and provide some
possible explanations for this phenomena. I'm really interested in
knowing what are your thoughts on this, so please open an issue in [this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues), or
reach [me in LinkedIn](https://linkedin.com/in/joao-o-santos) with your
ideas.

[Read full post](./humbling_statistics.html)



## [Job Crafting](./job_crafting.html)

**Date: 2023-02-14**

Have you tried tweaking your job to your needs and personal preferences?
In this post, I discuss how I have done it from my masters' until now.
I know I've been particularly privileged to have had people supporting
me throughout the process, and maybe even in having work and interests
that lend themselves well to job crafting. Still, I would like to
suggest you give job crafting a try, or at least consider it, as a means
of improving your work experience.

[Read full post](./job_crafting.html)



## [Is Your Problem Really Statistics?](./its_not_statistics.html)

**Date: 2023-02-13**

I follow up on [Sérgio Moreira's thought-provoking blog
post](https://spjmoreira.simple.ink/taking-it-easy-with-statistics-05d38c04b4f94f5db65e73b4f76cfa97),
with a long rant on philosophy of science and the research process. If
you're a student struggling with your thesis, I included a checklist
in the end to help you pinpoint what exactly is that you're having
trouble with. If you're an experienced researcher, but have never heard
of the new experimentalism, consider reading the severe testing section.
I hope you find those sections interesting enough to read the rest of
the post.

[Read full post](./its_not_statistics.html)



## [Informal Groups of Colleagues](informal_groups.html)

**Date: 2023-02-11**

I rant for way too long on the benefits of informal groups of colleagues
(broadly defined). I urge you to join or create a group if you're
a junior, and to encourage their formation, if you're a senior.
I provide some examples of groups I've attended and one that I'm still
attending, concluding with some tips and tricks for managing informal
groups.

[Read full post](./informal_groups.html)



## [Are Text-Based UIs Making a Comeback?](./text_uis.html)

**Date: 2023-02-10**

Are text-based user interfaces (UIs) making a comeback with chat bots
and chat-based AIs? As a command line `Linux` geek I'm left wondering if
we might be getting back to text-based UIs, with the rise of chat bots,
and AIs like chatGPT that let us chat with it.

[Read full post](./text_uis.html)



## [Blog Intro](./blog_intro.html)

**Date: 2023-02-08**

There are so many intelligent and meaningful blogs out there it's about
time we get some idiot to dumb it down. This entire blog will be
AI generated, in the sense that it will be generated by AnIdiot---me!
Since this blog is AI generated, I don't know what it will become in the
future but I hope to make many posts about MeaningLess topics. Still, AI
isn't perfect, and this idiot is no exception, so an interesting post
can still show up from time to time.

[Read full post](./blog_intro.html)



<!-- Template:

## [Post Title](./post_page.html)

**Date: 2023-mm-dd**

Summary

[Read full post](./post_page.html)

-->
