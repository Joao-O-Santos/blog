# MOC: How I Started Writing A Book

**Date: 2023-02-dd**


## Summary



## History



## My Teaching


### Past


### Present


### Future



## RUGGED



## License (GPL-v2.0)


## Milestones


### Alpha


### Beta


### V1.0



## Call for Collaborators


### Learners


### Revisers


### Translators


### Writers


### Instructors



## Thank you

**Thank you so much for reading!**

*If you would like to give some feedback please [open an issue on this
blog's GitLab](https://gitlab.com/joao-o-santos/blog/-/issues).*
